---
title: Initial Reply - For Non-German Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-German speakers"
sidebar: mydoc_sidebar
permalink: 226-Initial_Reply_For_Non-German_Speakers.html
folder: mydoc
conf: Public
ref: Initial_reply_non-native
lang: de
---


# Initial Reply - For Non-German Speakers
## First response, Email to Client if you're a non-German speaker

### Body


[masculine] Sehr geehrter [Client Name],

[feminine or plural] Sehr geehrte [Client Name/s],

mein Name ist [Your Name]. Ich arbeite bei der Digital Security Helpline von Access Now - https://accessnow.org/help

Wir haben Ihre Anfrage [Email Subject] erfolgreich erhalten. Leider spreche ich kein Deutsch. Wenn Ihre Anfrage dringlich ist, antworten Sie bitte auf diese Email und fügen Sie den Tag “URGENT” zur Betreffzeile hinzu. Wir bearbeiten dann Ihre Anfrage so schnell wie möglich.

Falls Sie meinen, dass wir auch auf Englisch [oder Französisch/Spanisch/Portugiesisch/Italienisch/Arabisch/Russisch] kommunizieren können, werde ich mich freuen, auf Ihre Anfrage unmittelbar zu antworten. 

Unsere deutschsprachigen KollegInnen werden in wenigen Stunden im Büro sein, dann bearbeiten sie Ihre Anfrage.

Mit freundlichen Grüßen,

[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)

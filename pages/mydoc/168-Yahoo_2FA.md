---
title: Secure Yahoo Account with 2-Step Verification
keywords: Yahoo, account security, verification, authentication, 2FA, 2-factor authentication
last_updated: July 19, 2018
tags: [account_security, articles]
summary: "The client's Yahoo account can be accessed with a normal password, which can be acquired through bruteforcing, phishing, etc."
sidebar: mydoc_sidebar
permalink: 168-Yahoo_2FA.html
folder: mydoc
conf: Public
lang: en
---


# Secure Yahoo Account with 2-Step Verification 
## Steps to enable 2-step verification for Yahoo accounts

### Problem

- The client's Yahoo account might have been hacked
- The client received a recovery link for their Yahoo account without having asked for it
- The client has received phishing emails trying to obtain their Yahoo password
- Emails have been sent from the client's Yahoo account without the owner's permission
- Worries about account access attempts from other devices which are not owned by the client


* * *


### Solution

Setting 2-step verification to secure the client's account is the best solution.

To enable 2-step verification for Yahoo, follow [these instructions](https://help.yahoo.com/kb/SLN5013.html).

The Electronic Frontier Foundation also has a [good guide with screenshots and tips](https://www.eff.org/de/deeplinks/2016/12/how-enable-two-factor-authentication-yahoo-mail).

If the client uses a client to access their email, after they have activated 2-step verification they should [generate a third-party app password to read their email](https://help.yahoo.com/kb/SLN15241.html).

Please note that on Yahoo, 2-step verification is SMS-based.


* * *


### Comments

Account sign-in on Yahoo can also be enabled through a Yahoo app on a phone. In that case, the client won't use a password to log in and will receive a one-time code on the app installed in their phone. With this method an SMS isn't needed, but access depends on the device where the app is installed. Read more on Yahoo Account Key [here](https://login.yahoo.com/accountkey/setup).

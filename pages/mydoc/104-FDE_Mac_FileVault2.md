---
title: Full-Disk Encryption on Mac with FileVault 2
keywords: FDE, full-disk encryption, Mac, FileVault 2, FileVault, device security
last_updated: Aug 07, 2018
tags: [devices_data_security, articles]
summary: "The client has an unencrypted Mac computer (OS X Lion - 10.7 - or later) and needs to protect sensitive files. A high-risk user has a Mac computer (OS X Lion or later) and doesn't know if full-disk encryption is enabled."
sidebar: mydoc_sidebar
permalink: 104-FDE_Mac_FileVault2.html
folder: mydoc
conf: Public
lang: en
---


# Full-Disk Encryption on Mac with FileVault 2
## Protect sensitive files in a Mac computer 

### Problem

- An at-risk user needs to protect sensitive files from theft or access by unauthorized persons.
- An at-risk user doesn't know how to securely store copies of important and/or sensitive files.


* * *


### Solution

FileVault 2 is an encryption tool inbuilt in Mac OS X that provides on-the-fly encryption and decryption for the content of the entire disk and/or home folder and sub-folders of a Mac OS X computer.

FileVault requires OS X Lion (10.7) or later. It uses the user's login password as the encryption passphrase. It uses the AES-XTS mode of AES with 128 bit blocks and a 256 bit key to encrypt the disk. Only unlock-enabled users can start or unlock the drive. Once unlocked, other users may also use the computer until it is shut down.

- [Apple's end-user guide](https://support.apple.com/en-us/HT204837)
- Send the setup instructions through the template in [Article #103: Setup FileVault 2 on a Mac](103-FileVault2_Mac.html)


* * *


### Comments




* * *

### Related Articles

- [Article #103: Setup FileVault 2 on a Mac](103-FileVault2_Mac.html)

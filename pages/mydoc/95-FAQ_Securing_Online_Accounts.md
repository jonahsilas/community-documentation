---
title: FAQ - Securing Online Accounts
keywords: authentication, verification, 2-factor authentication, two-factor authentication, 2-step verification, 2FA, multi-factor authentication, authentication security, securing accounts, social media, social networking
last_updated: November 9, 2018
tags: [account_security, articles, faq]
summary: "A client is concerned that one or more of their accounts might be or might have been compromised and needs basic recommendations on how to secure their online accounts."
sidebar: mydoc_sidebar
permalink: 95-FAQ_Securing_Online_Accounts.html
folder: mydoc
conf: Public
ref: FAQ_Securing_Online_Accounts
lang: en
---


# FAQ - Securing Online Accounts
## Basic security advice for mailboxes and accounts on social networking platforms and other online services

### Problem

Social media is crucial for many human rights activists to share their ideas, communicate with partners, and broadcast news. Their work and popularity can make them targets for hacking and account compromise.

This article focuses primarily on how to secure accounts on the main online services:

- Google
- Twitter
- Facebook

A case may start because:

- A client has received an unsolicited recovery link
- There is evidence that the account has been used by unauthorized persons without the owner's knowledge
- A client is concerned by attempts at accessing their account from devices they don't recognize
- A client is starting a campaign on social networking platforms and would like to prevent attacks against their account

In all such cases, we should work with the client to secure their account and make sure their account(s) cannot be accessed by unauthorized persons.


* * *


### Solution

Add layers of security for greater protection:

- Help the client to set up two-step verification (also called two-factor authentication)
- Provide recommendations for privacy and security settings
- Teach the use of strong passwords and password management
- Provide recommendations on online identity management

#### Two-Factor Authentication

For resources and other recommendations on two-factor authentication, see [Article #294: Recommendations on Two-Factor Authentication](294-FAQ-2FA.html).


#### Security Checkups

* [Google](https://security.google.com/settings/security/secureaccount)

* [Facebook](https://www.facebook.com/help/securitycheckup)


#### Security and Privacy Resources

* **Google**
    * [Google Security Tips](https://support.google.com/accounts/answer/46526?hl=en)
    * [Martin Shelton, "Newsrooms, let’s talk about G Suite"](https://freedom.press/training/blog/newsrooms-lets-talk-about-gsuite/) - an article on who can access data stored in GSuite. 

* **Facebook**
    * [Privacy](https://www.facebook.com/help/445588775451827/)
    * [Security](https://www.facebook.com/help/149190625213449/)
    * [WhatsApp security tips](https://www.whatsapp.com/security/)
    * [Instagram security tips](https://help.instagram.com/369001149843369)

* **Twitter**
    * [How to protect your personal information](https://support.twitter.com/articles/18368#)
    * [Twitter Security Tips](https://help.twitter.com/en/safety-and-security/account-security-tips)


#### Password Creation and Management

* Consider recommending a password management tool like KeePassXC.
    * For recommendations on team passoword managers, please see [Article #295: Recommendations on Team Password Manager](295-Password_managers.html).

*  Ensure the client knows how to create a strong password.
    * You can send them this [guide](https://ssd.eff.org/en/module/creating-strong-passwords)


#### Online Identity Management

If the client is starting a campaign from scratch or is managing public accounts for their organization from their personal accounts, and they fear attacks from trolls and mobs, encourage them to create a strategy for securely managing their online identities.

This is particularly required if the client's focus is on a topic that carries social stigma, like LGBTQ rights, feminist issues, etc.

1. Send them the guide for researching what can be found on them online: [Self-Doxing Guide](https://guides.accessnow.org/self-doxing.html).
    - If they find results that should be taken down, help them with the takedown requests.

2. Recommend they separate their personal accounts from their work accounts, possibly using a pseudonym for their private account.
    - This step should include connecting a different email account to their personal and work online accounts.
    - We should recommend they don't follow their work account from their personal account and vice versa, and they don't share the same contacts among the different accounts.
    - More information on managing different online accounts can be found in [this guide](https://gendersec.tacticaltech.org/wiki/index.php/Step_1#4._Creating_a_new_online_identity).

3. If the client's has state-level adversaries, encourage them to use anonymization tools for creating their most sensitive online accounts, and to separate them as much as possible from their real identity.
    - More information on anonymity tools can be found in [Article #175: FAQ - Circumvention &amp; Anonymity tools list](175-Circumvention_Anonymity_tools_list.html).


* * *


### Comments

If the account is known to be compromised, please see [Article #23: FAQ - Account Recovery and Deactivation](23-FAQ-Account_Recovery_and_Deactivation.html).


For more tips on Google Drive, see [Article #146: Secure File Sharing on Google Drive](146-Secure_File_Sharing_GDrive.html).


* * *


### Related Articles

- [Article #294: Recommendations on Two-Factor Authentication](294-FAQ-2FA.html)
- [Article #295: Recommendations on Team Password Manager](295-Password_managers.html)
- [Article #175: FAQ - Circumvention &amp; Anonymity tools list](175-Circumvention_Anonymity_tools_list.html)
- [Article #23: FAQ - Account Recovery and Deactivation](23-FAQ-Account_Recovery_and_Deactivation.html)
- [Article #146: Secure File Sharing on Google Drive](146-Secure_File_Sharing_GDrive.html)

---
title: Safe Browsing Practices and Plugins
keywords: browser, privacy, Firefox, Chrome, Safari, HTTPS Everywhere, Ublock Origin, NoScript, ScriptSafe, Privacy Badger, Tor, Tor Browser, VPN
last_updated: Aug 02, 2018
tags: [browsing_security, articles]
summary: "A client is inquiring about best practices when browsing online."
sidebar: mydoc_sidebar
permalink: 212-Safe_Browsing_Practices.html
folder: mydoc
conf: Public
lang: en
---

# Safe Browsing Practices and Plugins
## Best practices, tools, and application that can provide a more secure browsing experience for Helpline clients

### Problem

Online users have all experienced issues with adware, pop-ups, viruses, and other malicious content from the web. Tracking users and censorship over a specific region or country are also common practices. These threats may make our users feel unsafe. The following solutions will address the majority of their concerns.

These include the following:

- Protection from:
    - Adware/Popups
    - Scripts, malware and drive-by downloads
    - Web/URL redirects
    - Browser changes (settings, home pages, bookmarks, add-ons)

- Providing:
    - Protection from tracking
    - Anonymity
    - Censorship


* * *


### Solution

1. It is always recommended to check if the client's browser is up-to-date. This can be done by visiting [this website](https://browsercheck.qualys.com). If an update is required, try fixing the issue by clicking the “FIX IT” button in the web page with the results. This will help the client download or install the latest version for their browser or plugins.

2. Force HTTPS on the browser by installing the [HTTPS Everywhere extension](https://www.eff.org/https-everywhere)

3. Block ads and scripts:
    - Ublock Origin - Available in the Chrome and Firefox Webstore
    - NoScript (Firefox) &amp; ScriptSafe (Chrome) - *please note that these extensions can change dramatically the browser experience and should only be used by high-risk users*

4. Avoid Tracking
    - [Disconnect](https://disconnect.me/disconnect)
    - [Privacy Badger](https://www.eff.org/privacybadger)
    - [Do Not Track Me](https://www.abine.com/dntdetail.php)

5. Anonymity (for more information and the risks implied, see [Article #175: FAQ - Circumvention &amp; Anonymity tools list](175-Circumvention_Anonymity_tools_list.html)
    - Tor Browser (https://www.torproject.org)

6. Other Tools:
    - [Web of Trust](https://www.mywot.com) - a free browser extensions, mobile app and API that lets the user check if a website is safe before they visit it.

7. Browsing Best Practices &amp; Browser Plugins Review:
    - Always make sure that the browser you use is updated along with its extensions (Flash, Java, etc.)
    - Always review the browser plugins you have installed, remove unnecessary add-ons and plugins.

8. Review security hygiene with the user, so they continue to update their software, keep an eye out for suspicious links, and avoid downloading unsafe and unnecessary content.


* * *


### Comments

If the client uses Google services, we can suggest to use Chrome for accessing Google services, because Google's certificate fingerprints are pinned inside the code base.

Firefox also features [certificate pinning](https://bugzilla.mozilla.org/show_bug.cgi?id=744204)

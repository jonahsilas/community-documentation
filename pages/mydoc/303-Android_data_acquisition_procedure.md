---
title: Android Devices Data Acquisition Procedure
keywords:  mobile, forensics, malware, data acquisition
last_updated: January 30, 2019
tags: [forensics, articles]
summary: "The Helpline has come into possession of a Android device that could be infected or can be use as digital evidence."
sidebar: mydoc_sidebar
permalink: 303-Android_data_acquisition procedure.html
folder: mydoc
conf: Public
lang: en
---

# Android Devices Data Acquisition Procedure
## How to perform a deep analysis of the data contained in an Android device

### Problem

In order to perform a deeper analysis of the data contained in a mobile device (for the purpose of malware analysis or digital evidence gathering), different forensic techniques to acquire the data from the device should be applied, depending on the model, brand, structure, features and operating system version of the device, among other factors.


* * *


### Solution

1. Define the aim of the investigation:

    - **Case 1:** Acquire and preserve evidence to present in a court: please check with the Helpline management to decide how to proceed with this case. Probably in such cases you will need to reach out to a company specialized in forensics who has experience with the laws of the specific country.

    - **Case 2:** Malware analysis or other scenarios: depending on the situation, it may be sufficient to acquire a partial image of the device, or to just check which packages are installed and download a copy from the device. In other cases, a complete physical image of the device will be required.

2. Check the conditions of the device and record them in writing. Review the following points:

    - How was the device delivered to the Helpline? Was it sent via email or handed over by someone? Was it wrapped and correctly labeled? Was it contained in a Faraday bag?
    - Physical conditions of the device:
        - Was it off or on when handed over? If the device was on, what services were running and what applications were installed? Were Bluetooth, Wi-Fi or cellular data enabled?
        - Is the device complete and in perfect conditions? For example: does it have all its parts, like the battery, or is any part of the device broken? Is an SD card and/or a SIM card inserted in the device? 

3. Before proceeding with the data acquisition, gather the following information in the relevant ticket:

    - Information gathered from the client (if the client has this information, otherwise we should get it from the device).
    - General description of the device: model, brand, operating system version.
    - Reason for the device analysis: state the reasons why this image should be presented to a court, or write a description of the strange behavior that makes the client believe the device may be infected.
    - Add any available evidence (screenshots, documents, etc.).
    - In case of suspected malware infection, ask the client about possible infection vectors: was the device connected to an unsafe network? Did the client receive a suspicious message? Did the client send the device to a repair center? Did the client insert an unknown SD card or connect the device to an unknown computer or other device? Was the device seized, lost, or stolen and then recovered? What apps had the client downloaded recently and from were (official/unofficial app market, APK sent or downloaded from a suspicious source).
    - Ask the client to list the apps they installed and any sensitive information stored in the device.
    - Ask the client about backup services or apps enabled in the device.

    - Smartphone settings:
        - Is the installation of third-party apps enabled? (Go to "Settings" -&gt; "Security", and check if the "Unknown sources" option is checked).
        - Is the device rooted?
        - Is encryption enabled for the internal storage, SIM Card, and SD card of the device?

    - Check if the device is locked, encrypted and/or rooted:
        - If locked or encrypted, is it possible/appropriate to request the owner to provide the passcode for the device?
        - To check whether an Android device is rooted, check if Kinguser or SuperSU is installed. Kinguser and SuperSU are apps that manage your root access; they will be installed if a device has been rooted.

    - Record the brand, model, and operating system version of the device and check the settings and hardware:
        - How to identify the model: in most cases, depending on the brand, the model can be found on the back of the device or under the battery. Just in the cases where accessing the device is possible and does not interfere with the investigation, this information can be found in "Settings" -&gt; "About Phone".
        - If the device is a Samsung: check if the Common Criteria mode is available for that model (look for the model in [this list](https://support.samsungknox.com/hc/en-us/articles/115015114407-Guidance-documents-for-Common-Criteria-evaluation)) and if the [KNOX](https://www.samsungknox.com/en/knox-platform/knox-security) Workspace is configured in this device.
        - Check if this model has [JTAG test pins](https://www.xjtag.com/about-jtag/what-is-jtag/).
        - Check if the [chip-off technique](https://www.digitalforensics.com/blog/chip-off-technique-in-mobile-forensics/) is possible in this model (some models may have hardware security upgrades such as Secure Element, Trusted Execution Environment or Hostbased Card Emulation that make it difficult to apply the chip-off technique).
        - Check if disk encryption is enabled: most newer models have full-disk encryption enabled by default, while some models may not support encryption at all.
        - Get all the information related to the configuration and structure of the device (memory type, internal data management, memory chips location and type, etc.).

4. Please read [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html) and follow all recommendations included there when handling the device. You should also apply the following preventative measures:

    - Keep in mind the  order of volatility and other best practices listed [here](https://digital-forensics.sans.org/blog/2009/09/12/best-practices-in-digital-evidence-collection). If the device is running when it is delivered to the Helpline and it is rooted, try to extract the memory by following the procedure illustrated in [Article #???: Memory acquisition with LiME](xxx-Memory_acquisition_with_Lime.html). If you decide not to get the memory dump, justify this decision in the final report.
    - Keep the device disconnected from the Internet (in some cases, data can be remotely wiped). If required, connect it using a testing network especially set up for the data acquisition and analysis. Never connect the device to your office/co-working space/personal network.
    - Remember to keep the device isolated, immediately get the image of the device and turn it off. This is the safest action to preserve the evidence, and if you have the access code, there won't be a problem with the device encryption.
    - Don't connect the device or mount the SD Card found in the device directly to any laptop. In order to perform any data acquisition, you should prepare a forensic laptop or device, with the required write blockers mechanism and applications (you can send the information via `netcat` to this forensic laptop). 
    - Remember to sanitize the external medium you are going to use to store the logs and image before performing any action. Also remember to correctly label it and use a volume formatted to ext or NTFS as the destination media (make sure the volume has enough space for the device image and related logs/files, which will be as large as the device storage). As an extra security measure, encrypt the external device or create an encrypted container to save all the acquired data.
    - Most forensic tools or toolkits require a rooted device. The rooting procedure will depend on the model, brand, and OS version, and should be approached carefully, as it could break the device. Because of this risk, first get an image and all possible information *without* rooting the device. The procedure illustrated in [Article #305: Data Acquisition Using Android Debug Bridge (ADB)](305-Data_acquisition_Android_Debug_Bridge.html) can be used in this case as well. Once you have this image, you can proceed with rooting the device (at this point you will have an image backup in case something goes wrong with the rooting process). Look online for the adequate procedure for that specific device and request further assistance for a more experience security incident handler if required. Also, don't forget to record in your report the detailed process followed when backing up or rooting the device (both procedures alter the timestamps and other meta data providing false information when it comes to the data analysis, since all these steps will be reflected in the logs, so please don't forget to document it). If rooting is not possible or not required, just perform the data acquisition (remember to check if the procedure is updated and the recommended tool supports the OS version, structure, etc., of the device).
    - Prior to engaging in any mobile forensic technique, a solid understanding of key characteristics of the mobile device structure is necessary to properly and successfully pursue these techniques. As a report appendix, gather all the information collected and create a profile of the device. This information will help you select and justify the data acquisition tool or technique.

5. Data acquisition procedure:

    - If an SD card is inserted in the device, perform a byte-copy image of the SD card using a forensic distro and dc3dd: follow the instructions detailed in [Article #???: How to create a byte-copy image of a SD card using forensic distro and dc3dd](xxx-SD_card_byte_copy.html).
    - If a SIM card is inserted in the device, perform a byte-copy image of the SIM Card.
    - Select the procedure: based on the brand, model, structure, and features of the device, define what data acquisition technique and tool you will use using this table:


    | Technique | Type | Description | Issues | Special training or skills required | Works with encrypted devices? |
    |-----------|------|-------------|--------|-------------------------------------|-------------------|
    | Chip-off acquisition | Physical Data Acquisition | De-soldering storage for off-device reading |Requires physical access to underlying storage media, and cannot deal with the increasing use of encryption on storage devices. | Both techniques require highly specialized training and skills for repair, dismantling, and chip removal. Also, a clear understanding of block address remapping, fragmentation, encryption, and how to identify JTAG connectors on the motherboard and a strong knowledge in the device structure is required. | No |
    | JTAG Forensics | Physical Data Acquisition  | It is an interface for in-circuit reading of storage, often used during development and testing of a device, and can be used to communicate directly with the underlying storage media. | JTAG test pins can be hard to find and access on different devices, and can also be secured against unauthorised access or disabled by the vendor before shipping. | Same as above | No |
    | Commercial tools [3] | Physical and logical | Cellebrite UFED; Oxygen Forensics Suite; Parabens | Paid and not open source | Special training and equipment required. |  |
    | Rooting and exploitation solutions for recovering data by breaking the security of the device. | Logical Data Acquisition | Available tools: Magnet [4]; SAFT [5]; open source toolkits: Santoki [6]; Andriller [7] | A previous investigation should be done in order to determine what tool would work for this specific device model/structure. | The SIH should read the documentation for the selected tool and  perform the procedure with a testing cellphone before the real data acquisition. | It may depend on the feature/structure of the device. |
    | Android Debug Bridge (ADB) | Logical Data Acquisition | Device debug capabilities for forensic acquisition available in forensic distros [8] | ADB is a powerful debug interface supported by Android, but it is not enabled by default in most Android devices, nor does it give root access. | Same as above | Same as above |
    | Backup applications | Logical Data Acquisition | Backup solutions retrieving data through normal or rooted user (e.g.: Smart Switch) | Backup applications are rarely accessible to unauthenticated users and are often of limited use for forensics. | Basic knowledge on backup apps | Yes |
    | Other backup option | Logical Data Acquisition | NANDroid Backups [9]: these backups can be created by booting the device into a custom recovery mode and choosing the corresponding item in the menu. | NANDroid backup can only be created if the device is rooted, the bootloader is unlocked and custom recovery is installed. | Basic knowledge on rooting and booting procedures | No |
    | Non-standard use of firmware update protocols | Logical Data Acquisition | Installing a custom boot image in order to facilitate the injection of a forensic agent (Hawkeye, Lime [10]) | If Common Criteria mode is enabled in the device, the access to the firmware update mode is not possible. | High programming and reverse-engineering skills required. | No |
    
    Please analyze the information gathered, discuss the selected procedure with the Helpline management and team, and clearly justify the selection in the report. Consider that in most cases it will be necessary to use the procedures that will enable the access to the contents of partitions accessible only with root permissions, without having to remove the solid-state memory from the device.

    Perform the data acquisition following the procedure available (remember to check if the procedure is updated and the recommended tool supports the device OS version, structure, etc.).

    For data acquisition with ADB, please see [Article #305: Data Acquisition Using Android Debug Bridge (ADB)](305-Data_acquisition_Android_Debug_Bridge.html).

6. Create an encrypted container or encrypted medium with VeraCrypt (with a robust password), add all the information generated above in the container, and send it to the person/group who will perform the analysis. Send the password for the container using a different communications channel than the one used to share the container. Remember that during this data acquisition process sensitive personal data may be found in the byte-copy image and should be handled carefully.

7. Generate a report with all the case information (device description and conditions, device structure and features, data acquisition technique selected, data acquisition procedure, etc.). To create the report, follow the instructions in [Article #304: Mobile Data Acquisition Report Guidelines](304-Mobile_data_acquisition_report_guidelines.html).


* * *


### Comments

#### Notes

[1] [Forensics acquisition-Analysis and circumvention of Samsung secure boot enforced common criteria mode](https://www.dfrws.org/sites/default/files/session-files/paper_forensics_acquisition-analysis_and_circumvention_of_samsung_secure_boot_enforced_common_criteria_mode.pdf)

[2] [Survey on Android Forensic Tools and Methodologies](https://pdfs.semanticscholar.org/7f9c/b432a610d08dd4eda2cda5c17feacfa08863.pdf)

[3] Commercial tools:

- [Cellebrite UFED](http://www.hackingarticles.in/mobile-forensics-investigation-using-cellebrite-ufed/)
- [Oxygen Forensics Suite](https://www.forensicswiki.org/wiki/Oxygen_Forensic_Suite_2014)
- [Parabens](https://paraben.com/digital-forensic-software/) 

[4] [MAGNET](https://www.magnetforensics.com)

- [Example](https://www.magnetforensics.com/mobile-forensics/image-smartphone-magnet-acquire/)

[5] [SAFT](http://www.signalsec.com/saft/)

[6] [Santoku](https://santoku-linux.com/about-santoku/)

- [Example](https://resources.infosecinstitute.com/android-forensic-logical-acquisition/)

[7] [Andriller](https://www.andriller.com/)

- [Example](http://www.hackingarticles.in/forensics-investigation-of-android-phone-using-andriller/)

[8] DEFT 8.1 - Full support for Android and iOS 7.1 logical acquisitions (via libmobiledevice &amp; adb)

[9] [NANDroid backup definition](https://searchmobilecomputing.techtarget.com/definition/NANDroid-backup)

- [Example](https://www.makeuseof.com/tag/what-is-a-nandroid-backup-and-how-exactly-does-it-work/)
 
[10][Memory dump with Lime](https://github.com/504ensicsLabs/LiME/tree/master/doc)

- [Data Acquisition,Live imaging](https://freeandroidforensics.blogspot.com/2014/08/live-imaging-android-device.html)


#### References

- [Guide to Integrating Forensic Techniques into Incident Response](https://ws680.nist.gov/publication/get_pdf.cfm?pub_id=50875)
- [Forensic blog: mobile phone forensics and mobile malware](https://forensics.spreitzenbarth.de/)
- [Imaging Android with root, netcat and dd](https://dfir.science/2017/04/Imaging-Android-with-root-netcat-and-dd.html)
- [Netcat-Forensic Wiki](https://www.forensicswiki.org/wiki/Netcat)
- [Android File System explanation](https://infosecaddicts.com/perform-physical-acquisition-android-forensics) 
- [Android Forensic Labs examples](https://resources.infosecinstitute.com/android-forensics-labs/)
- [Webinar from Access Data: Introduction to JTAG &amp; Chip-off Mobile Forense](https://www.youtube.com/watch?v=_pkMuVBDBMs)
- [No Tool Fits All – Why Building a Solid Toolbox Matters](http://cyberforensicator.com/2018/07/15/no-tool-fits-all-why-building-a-solid-toolbox-matters/)
- [Samsung Devices](https://www.samsung.com/us/business/security/knox/assets/pdf/hhp-common-criteria-fips-sep-15.pdf)


* * *


### Related Articles

- [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html)
- [Article #???: Memory acquisition with LiME](xxx-Memory_acquisition_with_Lime.html)
- [Article #305: Data Acquisition Using Android Debug Bridge (ADB)](305-Data_acquisition_Android_Debug_Bridge.html)
- [Article #???: How to create a byte-copy image of a SD card using forensic distro and dc3dd](xxx-SD_card_byte_copy.html)
- [Article #304: Mobile Data Acquisition Report Guidelines](304-Mobile_data_acquisition_report_guidelines.html)

---
title: Content Restoration - Unsuccessful Escalation
keywords: censorship, content restoration, content moderation, unsuccessful escalation
last_updated: February 18, 2020
tags: [censorship_templates, templates]
summary: "Response to client after an unsuccessful attempt at escalation with social media platforms"
sidebar: mydoc_sidebar
permalink: 376-Content_Restoration_Unsuccessful_Escalation.html
folder: mydoc
conf: Public
ref: Content_Restoration_Unsuccessful_Escalation
lang: en
---


# Content Restoration - Unsuccessful Escalation
## Response to client after an unsuccessful attempt at escalation with social media platforms

### Body

Dear [$NAME],

My name is [$HANDLER_NAME], and I'm part of Access Now Digital Security Helpline. Thank you for reaching out.

We have reviewed and escalated your request to [list the platform]. However, in their response, they confirmed that the piece of content in question indeed violates the platform’s policies and therefore cannot be restored. Unfortunately, there is nothing else the Digital Security Helpline can do to assist you in this particular case. 

Please note that at Access Now we are concerned about the practices and policies governing content online. Part of our policy work includes providing recommendations to social media platforms to improve their response to content moderation issues, including ensuring that their decisions conform to the principles of transparency and proportionality, and that they provide direct access to an effective remedy for all users. The protection of your human rights, both online and off, is the utmost priority for us. Thus, we strongly encourage platforms to comply with international human rights law when crafting and deploying their content moderation practices. You can read more in detail about our work on a human rights-based approach to content governance here (hyperlink to our upcoming report) 

If you would like to provide feedback on the Digital Security Helpline please feel free to follow this survey: 

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Kind regards,

[IH's Name]


* * *


### Related Articles

- [Article #374: Content Moderation - Requests for Content Restoration](374-content_restoration.html)

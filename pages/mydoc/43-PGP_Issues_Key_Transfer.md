---
title: PGP - Issues with Key Transfer
keywords: email, PGP, key transfer, key management, troubleshooting, email security
last_updated: November 7, 2018
tags: [secure_communications, articles]
summary: "A GPG key pair has been transferred to a new machine, but the passphrase for the private key is not working in the new device."
sidebar: mydoc_sidebar
permalink: 43-PGP_Issues_Key_Transfer.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Issues with Key Transfer
## Troubleshooting issue with key not working after transfer to new machine

### Problem


The passphrase for a private PGP key is not working after the key pair was transferred from a different machine, but the key can be successfully used in the old device.


* * *


### Solution

Guide the client through the following steps:

#### In the old machine:

1. Change the passphrase for the private key in the old machine to a simpler one.
2. Copy the folder `~/.gnupg` to a secure USB stick.

#### In the new machine:

1. Close all of the programs related to gpg, including Thunderbird, OpenGPG, GPG Keychain, etc.
2. Back up the folder `~/.gnupg` in `~/.gnupgbkp`.
3. Delete the content of the `~/.gnupg` folder.
4. Copy the files in the `.gnupg` folder in the USB stick to the folder `~/.gnupg`.
5. Change the passphrase for the private key again. It should be a long and strong passphrase. 
6. Test the key in Thunderbird with the new passphrase.


* * *


### Comments



* * *


### Related Articles

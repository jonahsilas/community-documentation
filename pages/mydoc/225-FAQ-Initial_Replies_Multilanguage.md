---
title: FAQ - Initial Reply in Foreign Languages
keywords: initial replies, email templates
last_updated: July 20, 2018
tags: [helpline_procedures, articles, faq]
summary: "When a client sends their first email, we need to provide a human and understandable answer within 2 hours. In the initial reply we should inform the client that their request is being treated"
sidebar: mydoc_sidebar
permalink: 225-FAQ-Initial_Replies_Multilanguage.html
folder: mydoc
conf: Public
lang: en
---


# FAQ - Initial Reply in Foreign Languages
## This article regroups foreign language first reply templates and gives instructions on how to use them

### Problem

- The Handler on shift might not be able to recognize the language used in a client's first email.
- The Handler on shift does not master the client's language and is not able to communicate with them.


* * *


### Solution

#### 1. Recognize the language

If the Handler does not recognize the language an email is written in, they can submit the subject line as well as greeting and valediction to Google translate. If the email comes to us encrypted, only the subject line should be submitted.


#### 2. Reply with templates in foreign languages

Once the language of the email has been recognized, if it is one of the nine languages spoken by the Helpline staff, we can use the relevant template for the initial reply:

- **Arabic**: [Article #223: Initial Reply - For Non-Arabic Speakers](223-Initial_Reply_For_Non_Arabic_Speakers.html)
- **French**: [Article #220: Initial Reply - For Non-French Speakers](220-Initial_Reply_For_Non-French_Speakers.html)
- **German**: [Article #226: Initial Reply - For Non-German Speakers](226-Initial_Reply_For_Non-German_Speakers.html)
- **Italian**: [Article #267: Initial Reply - For Non-Italian Speakers](267-Initial_Reply_For_Non-Italian_Speakers.html)
- **Portuguese**: [Article #222: Initial Reply - For Non-Portuguese Speakers](222-Initial_Reply_For_Non-Portuguese_Speakers.html)
- **Russian**: [Article #224: Initial Reply - For Non-Russian Speakers](224-Initial_Reply_For_Non-Russian_Speakers.html)
- **Spanish**: [Article #221: Initial Reply - For Non-Spanish Speakers](221-Initial_Reply_For_Non-Spanish_Speakers.html)
- **Tagalog**: *NOT YET*

#### 3. Actions to take if the client responds with 'Urgent' tag

If the client responds indicating their case is urgent, please report it immediately to the 'Helpline - Urgent Cases' Signal group. Please indicate the language and subject of the case.

The 'Helpline - Urgent Cases' Signal group is a mechanism of immediate communication for urgent cases received in a language that is not spoken by the handler(s) currently on shift. It includes all incident handlers, shift leads and the Helpline director.


* * *


### Comments

#### Replies in languages we speak

A first reply should be sent to tickets in English and in other languages the Incident Handler masters. Templates for the initial reply in languages we speak are listed below:

- **English**: [Article #17: Initial Reply in English](17-Initial_Reply.html)
- **Arabic**: [Article #274: Initial Reply For Arabic Speakers](274-Initial_Reply_For_Arabic_Speakers.html)
- **French**: [Article #273: Initial Reply For French Speakers](273-Initial_Reply_For_French_Speakers.html)
- **German**: [Article #263: Initial Reply - For German Speakers](263-Initial_Reply_For_German_Speakers.html)
- **Italian**: [Article #264: Initial Reply - For Italian Speakers](264-Initial_Reply_For_Italian_Speakers.html)
- **Portuguese**: [Article #277: Initial Reply for Portuguese speakers](277-Initial_Reply_For_Portuguese_Speakers.html)
- **Russian**: [Article #265: Initial Reply - For Russian Speakers](265-Initial_Reply_For_Russian_Speakers.html)
- **Spanish**: [Article #266: Initial Reply - For Spanish Speakers](266-Initial_Reply_For_Spanish_Speakers.html)
- **Tagalog**: [Article #278: Initial Reply - For Tagalog Speakers](278-Initial_Reply_For_Tagalog_Speakers.html)



#### Questions on client's gender

Depending on the case, we may need to understand what the client's gender is. If so, we could add this final line to the templates, after "Please continue to include “[accessnow #ID]” in the subject line for all future correspondence about this issue":

    We would be grateful if you could tell us what pronoun/s you would like to be addressed with.


* * *

### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html.html)
- [Article #223: Initial Reply - For Non-Arabic Speakers](223-Initial_Reply_For_Non_Arabic_Speakers.html)
- [Article #220: Initial Reply - For Non-French Speakers](220-Initial_Reply_For_Non-French_Speakers.html)
- [Article #226: Initial Reply - For Non-German Speakers](226-Initial_Reply_For_Non-German_Speakers.html)
- [Article #267: Initial Reply - For Non-Italian Speakers](267-Initial_Reply_For_Non-Italian_Speakers.html)
- [Article #222: Initial Reply - For Non-Portuguese Speakers](222-Initial_Reply_For_Non-Portuguese_Speakers.html)
- [Article #224: Initial Reply - For Non-Russian Speakers](224-Initial_Reply_For_Non-Russian_Speakers.html)
- [Article #221: Initial Reply - For Non-Spanish Speakers](221-Initial_Reply_For_Non-Spanish_Speakers.html)
- [Article #274: Initial Reply For Arabic Speakers](274-Initial_Reply_For_Arabic_Speakers.html)
- [Article #273: Initial Reply For French Speakers](273-Initial_Reply_For_French_Speakers.html)
- [Article #263: Initial Reply - For German Speakers](263-Initial_Reply_For_German_Speakers.html)
- [Article #264: Initial Reply - For Italian Speakers](264-Initial_Reply_For_Italian_Speakers.html)
- [Article #277: Initial Reply for Portuguese speakers](277-Initial_Reply_For_Portuguese_Speakers.html)
- [Article #265: Initial Reply - For Russian Speakers](265-Initial_Reply_For_Russian_Speakers.html)
- [Article #266: Initial Reply - For Spanish Speakers](266-Initial_Reply_For_Spanish_Speakers.html)
- [Article #278: Initial Reply - For Tagalog Speakers](278-Initial_Reply_For_Tagalog_Speakers.html)

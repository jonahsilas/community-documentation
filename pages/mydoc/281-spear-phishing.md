---
title: How to Recognize Spear-Phishing and What to Do
keywords: spearphishing, phishing, targeted attacks, high-risk users
last_updated: April 15, 2019
tags: [vulnerabilities_malware, articles]
summary: "A user at risk has received a phishing message that seems targeted"
sidebar: mydoc_sidebar
permalink: 281-spear-phishing.html
folder: mydoc
conf: Public
lang: en
---


# How to Recognize Spear-Phishing and What to Do
## How to tell if a phishing message is targeted and what measures to take to warn clients and mitigate the attack

### Problem

A user at risk has received a phishing message by email or through a social
networking platform.

The message seems targeted and might be aimed at the single user or at a certain
group of the civil society.


* * *


### Solution

1. Analyze the case and possibly gather more information on similar incidents to
   evaluate if the phishing attack is targeted.

    Among the things we should consider to confirm the hypothesis of
    spearphishing, we should look for:
    
    - A customized text, containing information on the user or other signs of
      social engineering.
    - The usage of the user's language, especially if it's a dialect.
    - We can confirm that the phishing campaign is targeting in particular
      members of the civil society.
    - Any attempt to gather information on the target (phone number, email
      address, login id).

2. Do a search on MISP following the instructions in [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html) to check if similar attacks have been seen by the community.

3. If the suspicion of spear-phishing is confirmed by some of the above
elements, but only few cases have been observed, we can try to gather more
evidence by writing to intermediaries selected according to the area where the
campaign has taken place or, if the campaign is not restricted to one area, by
getting in touch with all intermediaries and partners that may be interested in
this kind of attack.

    When writing to intermediaries to gather evidence on a possible spear-phishing
campaign, we should use the template in [Article #280: Warning on Possible Spear-Phishing Attacks](280-spear-phishing-evidence-gathering.html).

4. If the phishing attack is confirmed:
    - If the attack is based on email messages, we should proceed as described
      in [Article #58: Client Receives a Suspicious/Phishing Email](58-Suspicious_Phishing_Email.html) to analyze the email and report any suspicious website or IP.
    - If the attack is launched through a social networking website, we should
      follow the specific escalation guidelines of the involved platform to help
      recover any hacked account, as well as report any malicious account
      involved. **In no case should we request the platform to disclose the
      attacker's IP address**.
    - Add the event to MISP following the instructions in [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html).
      


* * *


### Comments

- More information on targeted phishing attacks can be found in  Citizen Lab's
  [Targeted Threats report](https://targetedthreats.net/), specifically in the
  [appendix](https://targetedthreats.net/media/5-Appendix.pdf), which lays out
  an easy way to "grade" a communication on how targeted and customized an
  attack is.


* * *


### Related Articles

- [Article #280: Warning on Possible Spear-Phishing Attacks](280-spear-phishing-evidence-gathering.html)
- [Article #58: Client Receives a Suspicious/Phishing Email](58-Suspicious_Phishing_Email.html)
- [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html)
- [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html)

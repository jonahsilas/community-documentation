---
title: PGP - Transfer Key to New Machine
keywords: email, PGP, key management, transfer, export secret key, email security
last_updated: November 6, 2018
tags: [secure_communications, articles]
summary: "An old machine will no longer be in use and the owner's PGP key pair needs to be transferred to their new machine and wiped from the old device."
sidebar: mydoc_sidebar
permalink: 106-PGP_Transfer_Key_New_PC.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Transfer Key to New Machine
## How to securely transfer a PGP key from an old computer to a new one

### Problem

- The client needs to import their key pair to their new computer if they want to be able to keep decrypting and encrypting their emails in the new machine with their current keys.
- The key pair needs to be wiped in the old machine, as otherwise anyone having access to that device might also have access to the client's private key.


* * *


### Solution

1. Check the length of the client's current PGP key. If the length is 2048 bits or lower, we should recommend the client generates a new 4096-bit key for security reasons. This requires assisting the client to revoke the PGP key and create a new one. The client's contacts will then need to be notified to refresh their key chain.
    - See [Article #18: FAQ - PGP Setup](18-FAQ-PGP_Setup_Base_Article.html) for instructions on how to generate a new key pair.
    - See [Article #150: PGP - Revoking old key from key servers](150-PGP_Revoke_key.html) for instructions on how to revoke the old key.

2. If the length of the current key is 4096 bits or higher, we can proceed in guiding the client through the steps needed to export their key pair to their new machine, by sending them an email based on [Article #297: PGP - Transfer Key to New Machine - Email](297-PGP_transfer_key_email.html).

3. Wait for the client's encrypted reply and check that they can encrypt and decrypt emails in their new machine by replying to their email and making sure that they can read your encrypted message.


* * *


### Comments

If the client is using GPGTools + Apple Mail, a very similar export/import process can be followed using the GPG Keychain. More details can be found in [this guide](https://gpgtools.tenderapp.com/kb/gpg-keychain-faq/backup-or-transfer-your-keys).

#### Troubleshooting

If after moving the key pair to the new machine, the passphrase for the private key does not work, see [Article #43: PGP - Issues with Key Transfer](43-PGP_Issues_Key_Transfer.html).


* * *


### Related Articles

- [Article #18: FAQ - PGP Setup](18-FAQ-PGP_Setup_Base_Article.html)
- [Article #150: PGP - Revoking old key from key servers](150-PGP_Revoke_key.html)
- [Article #297: PGP - Transfer Key to New Machine - Email](297-PGP_transfer_key_email.html)
- [Article #43: PGP - Issues with Key Transfer](43-PGP_Issues_Key_Transfer.html)

---
title: Phishing - First Email
keywords: email templates, phishing, email headers
last_updated: August 8, 2019
tags: [phishing_suspicious_email_templates, templates]
summary: "First email, with precautions and instructions to get full headers"
sidebar: mydoc_sidebar
permalink: 57-Phishing-First_Email.html
folder: mydoc
conf: Public
ref: Phishing-First_Email
lang: en
---


# Phishing - First Email
## First email, with precautions and instructions to get full headers

### Body

Hi $CLIENT_NAME,

Thanks for reaching out to us in relation to the suspicious email you received.

[If reasons why the email is suspicious are not clear]
I will start analyzing the message you received as soon as possible. However, if you could please clarify why the email is suspicious to you, that would be helpful.

First of all, it would be interesting to know if the supposed sender of the email
you received is known to you. If they are known to you, was the email apparently
sent from their email address, or was their name and surname used but with a 
different email address?

[If the email contains attachments/links]
*Please do not open any link or attachment in your email.* Since this is a suspicious email, we need to work with caution. Therefore, please do not follow any link or download any attachment until we are able to verify the veracity of this communication.

To perform a full analysis of the email, we would need the full headers of the message. In this link you can find detailed instructions on how to obtain the email headers:  https://www.circl.lu/pub/tr-07/ for different mail clients. If you need any help, please let me know. 

I remain available for any doubts or questions. Please feel free to reach out to me if you need additional help. 

With best regards,
$HANDLER_NAME

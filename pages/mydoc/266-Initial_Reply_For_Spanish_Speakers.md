---
title: Initial Reply - For Spanish Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Spanish speakers"
sidebar: mydoc_sidebar
permalink: 266-Initial_Reply_For_Spanish_Speakers.html
folder: mydoc
conf: Public
ref: Initial_reply
lang: es
---


# Initial Reply in Spanish
## First response, Email to Client that speaks Spanish

### Body

Hola $ClientName,

Gracias por contactar a la Línea de Ayuda de Access Now (https://www.accessnow.org/linea-de-ayuda-en-seguridad-digital/), mi nombre es $IHName y estoy para asistirle.

Hemos recibido su solicitud y nuestro equipo está trabajando en el caso. 

Por favor tenga en cuenta que la Línea de Ayuda es operada por un equipo de profesionales en seguridad digital trabajando desde diferentes regiones y zonas horarias, por lo tanto, diferentes miembros del equipo pueden responder a sus mensajes dependiendo del día y la hora a las que nos contacte. En poco tiempo le estaremos enviando un correo con detalles adicionales o solicitando más información necesaria. 

Por favor utilice la etiqueta “[accessnow #ID]” en el asunto de los mensajes relacionados a este caso. Este es un número de referencia generado por nuestro sistema de administración utilizado para llevar el control de los casos

Saludos cordiales,

$IHName


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)

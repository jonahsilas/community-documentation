---
title: Removing Adware from a Windows Machine
keywords: adware, Windows, virus
last_updated: July 20, 2018
tags: [vulnerabilities_malware, articles]
summary: "The client's browser is showing ads that stop them from doing what they need to do, for instance when trying to access documents from Google drive"
sidebar: mydoc_sidebar
permalink: 134-Remove_Adware_Windows.html
folder: mydoc
conf: Public
lang: en
---


# Removing Adware from a Windows Machine
## How to get rid of adware that can harm the drive or browser of a Windows machine

### Problem

- Ads prevent the user from doing what they need to do. 
- It may cause other problems like spamming the user's contacts.


* * *


### Solution

- Remove and uninstall any unrecognized extension.
- Restore the default settings.
- Install an adware removal tool (for example [Malwarebytes AdwCleaner](https://www.malwarebytes.com/adwcleaner/)).
- Ask the client to provide us with the adware scan report.
- Review the log and provide the client with the right tips based on the scan report.
- Remove any suspicious file or program detected by the malware tool.


* * *


### Comments

If you suspect the client might have been targeted with other kinds of malware, also see [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html) and [Article #133: How to clean a malware-infected Windows machine](133-Clean_Malware_Windows.html).


* * *


### Related Articles

- [Article #133: How to clean a malware-infected Windows machine](133-Clean_Malware_Windows.html)
- [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html)

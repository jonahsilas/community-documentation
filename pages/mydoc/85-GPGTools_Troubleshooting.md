---
title: GPGTools Issues with Encrypting or Signing
keywords: email, PGP, GPGTools, GPG Suite, Mac, troubleshooting, email security
last_updated: November 6, 2018
tags: [secure_communications, articles]
summary: "A new installation of GPGTools with Apple Mail on a Mac computer does not allow encrypting or signing emails. In Preferences --&gt; GPGMail there is a green check saying GPG was set up, but encryption is not working."
sidebar: mydoc_sidebar
permalink: 85-GPGTools_Troubleshooting.html
folder: mydoc
conf: Public
lang: en
---


# GPGTools Issues with Encrypting or Signing
## PGP - troubleshooting if GPGTools for Mac does not allow encrypting or signing

### Problem


A client who has just installed GPGTools on their Mac computer is not able to use encrypt or sign their emails in Apple Mail although the installation was successful and the creation of the key was completed with no issues. 

One potential problem is that the email address used to create the UID for the PGP key and the email account are not consistent. A typo or mismatch will cause issues when trying to encrypt messages.


* * *


### Solution

Check the email address used to create the key and the existent email address in Apple mail. If necessary, contact the email provider to fix typos in the email address, or ask the client to generate a new PGP key if they still haven't used or uploaded to the key servers the one they just generated.


* * *


### Comments



* * *


### Related Articles

---
title: Guiding Questions for High-Risk Organisations
keywords: digital security assessment, high-risk organizations, questions, advanced threats, threat assessment
last_updated: July 20, 2018
tags: [organization_security, articles]
summary: "A high-risk organization is requesting assistance for dealing with advanced threats and a clear threat assessment is needed"
sidebar: mydoc_sidebar
permalink: 262-Guiding_Questions_High-Risk_Orgs.html
folder: mydoc
conf: Public
lang: en
---


# Guiding Questions for High-Risk Organisations
## Guidelines for initial advanced threat assessment

### Problem

- A high-risk organization suspects or knows they have advanced threats, but don't have a clear idea of what these threats may be and how to handle them.
- Based on the initial security assessment, the Helpline suspects the organization might be facing advanced threats and needs to investigate the situation to identify possible preventative or reactionary measures.

This article provides guidance to incident handlers for performing an advanced threat assessment for high-risk Helpline clients.


* * *


### Solution

The goals of an advanced threat assessment should be the following:

- Identify if there are indicators that tell us that the organization has been compromised or someone they know has faced advanced threats.
- Identify what threats they are dealing with.
- Prioritize what we should work on with the organization.
- Understand whether the Helpline has the capacity to address or understand the threat or if we need to refer the client to outside experts.


#### Guiding questions

*Before starting the individual interview, consider asking some of the questions included in [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html) to explore the client's specific context.*


1. Have you encountered suspicious messages, emails, etc. in the course of your work or personal life?

    ***If the answer is "yes":***

    1. Can you tell me about the suspicious message? What made you feel it was suspicious?
    2. What did you do when you received it? (i.e. who did you contact? did you click on it or download a file? did you follow the instructions?)
    3. Do you feel you are compromised now? How does this impact you?
    4. Can you share this message?

        *[See [Article #58: Client Receives a Suspicious/Phishing Email](58-Suspicious_Phishing_Email.html) for instructions on how to share the message.]*

2. Have you received any account notifications, such as SMS or emails notifying you of unauthorized access to your account (email, social media), an account being locked, suspicious activity on your account?

    *In case there is more than one suspicious event, ask questions 2 to 4 again.*


3. Has this happened to colleagues, peer organisations, community members, civil society actors (journalists, etc.)? [Add actors based on context research]

4. Have you ever experienced an incident or hack during the course of your work?

    ***If the answer is "yes":***

    1. Can you tell me about that event/incident/hack?

        *[i.e. who was involved, when it happened, what happened, was it personal or work-related? what were the consequences? (financial, physical, emotional, reputational)]*

    2. What did you do after? Who did you ask for help from?

    3. Do you have something that you can show us? [i.e. an email, screenshots, social network messages, the actual infected machine, message from the attacker, social network pages made by attackers, leaked information]

    4. Do you feel you are compromised now? How does this impact you?

    *In case there is more than one attack, ask questions 3, 4, 4.1, and 4.2 again.*

5. Has this happened to colleagues, peer organizations, community members, civil society actors (journalists, etc.)? [Add actors based on context research]

    *Ask questions 3, 4, 4.1, and 4.2, but adjusted to the new actor.*

6. Why do you think you are targeted?

* * *

*If the answer to question 4 ("Have you been hacked?") is "no" or "I don't know", the auditor should give an example of what an attack might look like, using a story of a similar organization that explains one or two examples:*

- DDoS attack
- Website defacement
- Spam and advertisement
- Malware
- Attachment that doesn't work
- Attachment that the antivirus software considers infected
- Attachment from unknown person or unexpected email
- Phishing
- Gmail reset password notifications
- Social networking hacks
- Blackmail and electronic threats
- Ransomware

7. Does any of this apply to you?

    *If they say no, then move on to other questions for the risk assessment included in [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html).*

    *If they say yes, then go through questions 4-6 again.*

For group interviews, see SAFETAG [Advanced Threat module](https://safetag.org/guide/#section4.13).


* * *


### Comments

#### Psychological Considerations

- Ask the staff to keep the stories generalized, not personalized during the organization interview
- Staff might be embarrassed to talk about an incident in front of colleagues, so it is better to run 1:1 interviews
- Staff might exaggerate or overestimate attacks due to lack of understanding of the attack and impact
- Staff might underestimate attacks due to overexposure to these hacks, other pressing challenges, or lack of understanding
- Incident handlers should listen and explain concepts, but not argue about the "seriousness" of the incident
- Don't correct the staff member if they describe the incident incorrectly
- Tread carefully, given the topic can be triggering or difficult


#### Operational Security

- The interview should happen through an end-to-end encrypted channel.
- Consider suggesting that the client does not have the conversation in the office, but in a trusted space.
- Consider suggesting the client to use a device different than their own, if you suspect their device may be infected.
- Consider suggesting the client to leave their own devices outside of the room where the interview is happening.


#### Other Considerations

- Before we start our assessment, it's a good idea to do a context research on similar organizations and possible attacks they faced, so as to identify potential threats our client might be facing.
- Before starting the interview process, read about known or common attacks that you can reference to remind staff and get the conversation started. Compile a story for each of the following advanced threats:
    - DDoS attack
    - Website defacement
    - Malware
    - Phishing
    - Blackmail - Electronic Threats
    - Ransomware
- In order for the stories to be compelling, they should be localized and the threats should reflect common challenges in their line of work.

Read article on [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html)

Reference materials can be found in [SAFETAG - Security Auditing Framework and Evaluation Template](https://www.safetag.org/), in particular in the [Advanced Threat module](https://safetag.org/guide/#section4.13).


* * *


### Related Articles

- [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html)
- [Article #58: Client Receives a Suspicious/Phishing Email](58-Suspicious_Phishing_Email.html)
- [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html)

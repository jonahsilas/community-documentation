---
title: Follow-up - Subcontact - Russian
keywords: follow-up, referrals, third-party consultants, email templates
last_updated: February 6, 2019
tags: [helpline_procedures_templates, templates]
summary: "Письмо клиенту об успешности обращения к третьей стороне"
sidebar: mydoc_sidebar
permalink: 317-Followup_Subcontact_ru.html
folder: mydoc
conf: Public
ref: Followup_Subcontact
lang: ru
---


# Follow-up - Subcontact
## Email to client to follow up on a referral to a third party

### Body

Здравствуйте,

Я [ИМЯ СОТРУДНИКА] из Службы поддержки по вопросам цифровой безопасности организации Access Now.

В настоящее время я работаю над оценкой эффективности предпринятых мер. Скажите, пожалуйста, удалось ли вам связаться с [ИМЯ КОНТАКТА], человеком, который [ЧТО ДОЛЖЕН БЫЛ СДЕЛАТЬ КОНТАКТ]?

С уважением,

[ИМЯ СОТРУДНИКА]

---
title: Content Removal - Out of Mandate
keywords: harassment, content moderation, content removal, remove accounts, takedown, out of mandate
last_updated: February 18, 2020
tags: [harassment_templates, templates]
summary: "Template to communicate to the client that their request for content removal is out of mandate"
sidebar: mydoc_sidebar
permalink: 373-Content_Removal_Out_of_Mandate.html
folder: mydoc
conf: Public
ref: Content_Moderation_Out_of_Mandate
lang: en
---


# Content Removal - Out of Mandate
## Template to communicate to the client that their request for content removal is out of mandate

### Body

Dear [$NAME],

My name is [$HANDLER_NAME], and I'm part of Access Now Digital Security Helpline. Thank you for reaching out.

While we understand the hardship you are going through, unfortunately, we cannot assist you with your request, as we have determined that it falls outside of the scope of assistance of the Digital Security Helpline. You can find our terms and the scope of our work described here. 

Also, please note that Access Now’s work includes recommendations to social media platforms  to improve their response to content moderation issues, including ensuring that their decisions conform to the principles of transparency and proportionality, and that they provide direct access to an effective remedy for all users. The protection of your human rights, online and off, is the utmost priority for us. Thus, we strongly encourage platforms to comply with international human rights law when crafting and deploying their content moderation practices. You can read in detail about our work on human rights-based approach to content governance here [hyperlink to our upcoming report]. 

If you would like to provide feedback on the Digital Security Helpline please feel free to follow this survey: 

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Kind regards,

[IH's Name]


* * *


### Related Articles

- [Article #371: Content Moderation - Requests for Content Removal](371-content_removal.html)

---
title: Set-up Instructions for XMPP and OTR on Linux, Windows and macOS
keywords: encrypted chat, secure IM, secure instant messaging, encryption, end-to-end encryption, XMPP, OTR, Linux, Windows, macOS, Mac, set-up, account
last_updated: August 26, 2019
tags: [secure_communications, articles]
summary: "A client has identified XMPP+OTR as the best option for securing their chat communications and has already installed Pidgin and OTR or Adium. They now need guidance for setting up their account and OTR."
sidebar: mydoc_sidebar
permalink: 208-XMPP-OTR_Setup.html
folder: mydoc
conf: Public
ref: XMPP-OTR_Setup
lang: en
---


# Set-up Instructions for XMPP and OTR on Linux, Windows and macOS
## How to configure an account and set up OTR in Pidgin/Adium for secure instant messaging communications

### Problem

Often clients rely on insecure services like Skype and Facebook to communicate sensitive information over instant messaging. Using insecure channels to communicate may cause data leaks and put sensitive information in the wrong hands.

If the client decides to use [XMPP](https://en.wikipedia.org/wiki/XMPP) as a secure alternative, they might need guidance on how to set up their account on an XMPP client installed in their computer and enable [OTR](https://otr.cypherpunks.ca/) to make end-to-end encryption possible.


* * *


### Solution

If the client has not already installed Pidgin and the OTR plugin or Adium in their computer, please refer to [Article #207: Install XMPP+OTR on Linux, Windows and macOS](207-XMPP-OTR_Install.html) to guide them through the installation.


#### Account Set-Up

##### Pidgin

The first time you open Pidgin, it will take you to the Account Setup Wizard. You may add an account by clicking the "Add" button. If you do this at a later stage, you can go back to the account set-up wizard by clicking on "Accounts" &gt; "Manage Accounts".

After you click "Add", an "Add Account" window with values to fill out will appear.

1. In the "Basic" tab:
    - Protocol: select XMPP in the drop-down menu.
    - Username: enter your XMPP username
    - Domain: enter your XMPP domain. If you're using GTalk, this will be the part of your email address after the `@` - for example gmail.com
    - Resource: you can leave this blank
    - Password: enter the password for your XMPP account.
    
        *Please note that if the client's XMPP account is connected to an email account where 2-factor authentication is enabled, they will need to generate an app-specific password for Pidgin. You can send them [this link with instructions](https://support.google.com/accounts/answer/185833?hl=en).*

2. In the "Advanced" tab:
    - Make sure that "Require encryption" is selected in the "Connection security" field.
    - In the "Connect server" field, enter your XMPP server. If you're using GTalk, it will be `talk.google.com`.

3. Click "Save". In the "Accounts" window, you will see your account. If the box next to it is checked, it means you are connected.

The first time you connect, a prompt might appear asking if you want to accept a certificate. Accept it and you will connect to your XMPP server.

- [How to set up Pidgin for GTalk](https://support.google.com/a/answer/49147?hl=en) - although using GTalk is deprecated by Google, in our experience this still works.

- [Pidgin end-user documentation](https://developer.pidgin.im/wiki/Using%20Pidgin)


##### Adium

Adium is similar to Pidgin, but simpler. When you open it for the first time, you will see a Setup Wizard that will allow you to add an account. You can also do this by going to the Adium menu at the top of your screen and click “Adium” and then “Preferences”. This will open a window with another menu at the top. Select “Accounts,” then click the “+” sign at the bottom of the window.

- In the Adium Wizard, select "Jabber". If you're using GTalk, select "Google Talk" instead.
- Add your user ID and your password.

    *Please note that if the client's XMPP or GTalk account is connected to an email account where 2-factor authentication is enabled, they will need to generate an app-specific password for Pidgin. You can send them [this link with instructions](https://support.google.com/accounts/answer/185833?hl=en).*


#### OTR Set-Up

##### Pidgin

- To enable OTR encryption, go to "Tools" -&gt; "Plugins" and check "Off-the-Record-Messaging"
- In the plugins window, search for "Off-the-Record Messaging" and check the box on the left to enable the plugin.
- With the "Off-the-Record-Messaging" still selected, click the "Configure Plugin" button at the bottom of the window.
- In the "My private keys" section, you can see a message that says "No key present".
- To generate your encryption keys, select your account in the drop-down menu, then click "Generate".
- A window will open and inform you that it has started generating a key for the account. Click "OK" after it says "Done".
- You can now see a string of letters and numbers. This is your OTR fingerprint.
- In the "Default OTR Settings" section of the same window, make sure that "Enable Private messaging", "Automatically initiate private messaging", and "Don't log OTR conversations" are checked.
-  When done, click "Close" to close the "Off-the-Record Messaging" window.
- At this point you can start an encrypted chat with your contacts.

###### Adium

Instructions on how to generate an OTR key in Adium can be found [in the official documentation](https://adium.im/help/pgs/AdvancedFeatures-OTREncryption.html).


#### Start an Encrypted Chat and Authenticate Your Contact

##### Pidgin

Identify one contact in your buddy list who you know has OTR set up in their device. Double-click the contact to start a conversation.

To enable encrypted chat:

1. In the conversation window, there is an option right above the text box on the right that says "Not Private".
2. Right-click the "Not Private" box and select "Start private conversation".
3. In the conversation window, you will see automatic messages saying that a private conversation has started, after which the box in the bottom right corner will turn to "Unverified".
4. To be sure about your contacts' identity, it's always a good idea to [authenticate them](https://otr.cypherpunks.ca/help/3.2.0/authenticate.php?lang=en), i.e. to verify that they are who they say they are. Right-click the "Unverified" box and select "Authenticate Buddy".
5. A window will open asking you "How would you like to authenticate your buddy"? You can use one of the following methods:
   	 - Shared Secret: Include a line of text that you agreed on with your contact before starting the encrypted chat. Note: this text should not be sent over an unencrypted channel, so it's better to discuss this in person ahead of time, or send it through an encrypted email message or another end-to-end encrypted communication tool.
    - Manual Fingerprint: This is useful only if you have your contact's fingerprint. Not recommended if your contact connects through more than one device.
    - Question and Answer: You can verify your contact's identity by asking a question and relying on them to answer correctly. This can be any information that only you and your contact know. This is case sensitive, so you can put in your note whether the answer should be in uppercase or lowercase letters.

    Once you have successfully authenticated your contact, a window will show up with the message "Authentication Successful" and the button that used to read "Unverified" will turn green and read "Private". You can now be sure that your instant messaging communications with that contact are secure.


##### Adium

1. Identify one contact in your buddy list who you know has OTR set up in their device. Double-click the contact to start a conversation, then click on the open lock icon in the upper right-hand corner of the chat window, and select "Initiate Encrypted OTR Chat". The previously unlocked icon is now locked - this means your chat is now encrypted.
2. You will be prompted to verify the fingerprint of your contact.  A window will open, showing YOUR key and the OTHER USER's key. You have to make sure that the keys shown by Adium match, by checking with your contact through another secure communication channel or face to face. If you are certain of its trustworthy origin click "Accept".


* * *


### Comments

If your client does not have Pidgin and the OTR plugin or Adium installed in their
computer, please refer to [Article #207: Install XMPP+OTR on Linux, Windows and macOS](207-XMPP-OTR_Install.html).

Also see the EFF's Security Self-Defense Guides on setting up OTR, with instructions
for installation and set-up for Adium on macOS and Pidgin+OTR on Linux:

- [macOS](https://ssd.eff.org/en/module/how-use-otr-macos)
- [Linux](https://ssd.eff.org/en/module/how-use-otr-linux)

If the client needs to use XMPP and OTR in a mobile device, please see the [section on XMPP](141-Secure_Chat_Mobile.html#xmpp) in [Article #141: Secure Chat Tools for mobile devices](141-Secure_Chat_Mobile.html).


* * *


### Related Articles

- [Article #207: Install XMPP+OTR on Linux, Windows and macOS](207-XMPP-OTR_Install.html)
- [Article #141: Secure Chat Tools for mobile devices](141-Secure_Chat_Mobile.html)

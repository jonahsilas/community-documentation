---
title: Host-Based Live Forensics on Linux/Unix
keywords: forensics, built-in tools, command line, GNU/Linux, Linux, Unix
last_updated: February 10, 2020
tags: [forensics, articles]
summary: "The Helpline needs to perform live forensics on a Linux/Unix machine using built-in tools."
sidebar: mydoc_sidebar
permalink: 368-Live_Forensics_for_Linux.html
folder: mydoc
conf: Public
lang: en
---


# Host-Based Live Forensics on Linux/Unix
## How to perform live forensics on a Linux/Unix machine using the available built-in tools 

### Problem

In some instances, a Linux/Unix  machine may need to be isolated from the shared network or internet access point it is usually connected to due to its compromised state. This article applies to a similar scenario, where we have limited resources to conduct forensic analysis and have to rely on the built-in tools available in the device.

* * *


### Solution

Below is the general methodology we can follow when conducting live forensics on a Linux/Unix machine.


#### 1. Unusual Network Entries

There are protocols in a Linux/Unix machine that can be exploited/abused by attackers both on the client and server side. The following commands will list the sessions and connection that we can use for our initial investigation:

    # netstat -nap

Displays listening UDP and TCP ports and remote IPs. Adding -na will show all the connections without resolving the domain and adding -p will show the program name and the PID.

Note: PIDs are useful for investigating the program processes.

    # lsof -i

Stands for list open files - this command will give you a comprehensive list of all open files, be it a regular file, a library, a network file, or a data stream. Executing these commands as root should show other entries launched by the root or privileged users.

Another command useful for host-based investigation is `dmesg` piped to a `grep` command in order to find if the machine is set on promiscuous mode:

    # dmesg | grep -i promisc

If this mode is on (returns result), this means that the machine is sniffing the network. This should be a good indicator that the machine is compromised and is sniffing the rest of network to find valuable information or to compromise more devices.


#### 2. Unusual Processes

Launch the following command to list all running processes:

    # ps -aux

Please note however that this command should be compared to a baseline of normal running processes (sysadmins may have a prior list of processes created during auditing processes). A list of processes is hard to be found for workstations, especially if the device is also used for non-work related tasks.

Piping the output of `ps -aux` to a `grep` command referred to a PID will allow us to focus only on one process:

    # ps aux | grep [PID]


The command format and arguments are very similar to those of the `netcat` command that opens a listening connection. 

Using `lsof` with the -p flag plus a PID will display results for all the files opened by a specific process:

    # lsof -p [PID]


#### 3. Unusual Debian Packages (only for Debian-based systems)

Launch the following command:

    # debsums -all

An attacker with root privileges could change Debian package to makes their script look legitimate. The `debsums` command can identify such activities by checking the integrity of all installed Debian packages against a local list of hashes under the default file `/var/lib/dpkg/info/*.md5sums`.

#### 4. Unusual Files

Launch the following commands:

```
find / -uid 0 -perm -4000 -print

find / -size +10000k -print 

find / -name " " -print 

find / -name ".. " -print
```

The presence of unusual files could be an indicator of data exfiltration. Through the above commands, we aim to find respectively: (1) SUID files, (2) large files, and (3) files named with a space and two points.

#### 5. Unusual Scheduled Tasks

In Linux systems, scheduled tasks are created using crons. This is a way for threat actors to ensure that the malicious program will be executed again even if the process is found and killed.

Use the following commands for investigating unusual system-wide cron jobs:

    crontab -u root -l

    cat /etc/crontab

    ls /etc/cron


#### 6. Unusual Accounts

Sort all users in `/etc/passwd` by their User ID or UID value and check especially the users with a UID lower than 500, as these users usually have more sensitive permissions.

    sort -nk3 -t: /etc/passwd | less

Orphaned files are indicators of deleted users that could have been created by an attacker who then deleted these users to hide their traces. To identify such files, launch this command:

    find / -nouser -print


* * *


### Comments

Please check [this presentation](https://drive.google.com/file/d/1bdyQlJLDWw5aq1EP_IhYTW2t-tOQh3tu/view?usp=sharing) to see typical outputs of most of the commands listed in this article. 


* * *

### Related Articles

---
title: Warning Before Gmail Account Deactivation
keywords: Google, Gmail, escalations, account deactivation, account recovery
last_updated: September 18, 2019
tags: [account_recovery_templates, templates]
summary: "Template to warn clients that a recovery method needs to be set up to recover a deactivated account"
sidebar: mydoc_sidebar
permalink: 362-Warning_Before_Gmail_Deactivation.html
folder: mydoc
conf: Public
ref: Warning_Before_Gmail_Deactivation
lang: en
---

# Warning Before Gmail Account Deactivation

## Template to warn clients that a recovery method needs to be set up to recover a deactivated account

### Body

Dear $Client_name / $Intermediary_name,

We are following up on your request to secure the Gmail account of $Client_name $email_adress. Once this process is completed, access to the account will be blocked and no one will be able to log in. If, eventually, the account needs to be re-enabled, please let us know and we will help initiate the recovery process. 

Please be aware that in order to recover the account the owner needs to use the recovery options that are currently configured. These are normally listed under the "Ways we can verify it's you" in the account security settings at [this link] (https://myaccount.google.com/security)

Unfortunately, if the owner is unable to verify the ownership through the standard mechanisms or existing recovery information, it will be very difficult to recover the account, so we advise that this recovery information is valid and accessible by the account owner.

Given these considerations, we kindly ask you to confirm your request to secure the account with assurance that the owner has control on their recovery information before we move forward. 

Thank you for your understanding. Please do not hesitate to let us know if you have any questions.

Kind Regards,

$IH_name

* * *


### Related Articles

-  [Article #135: FAQ - Google Escalation Guidelines](135-FAQ-Google_Escalation_Guidelines.html)

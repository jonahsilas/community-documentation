---
title: Russian - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 14, 2019
tags: [helpline_procedures_templates, templates]
summary: "Шаблон письма для сбора обратной связи от клиентов"
sidebar: mydoc_sidebar
permalink: 350-ru_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: ru
---


# Russian - Feedback Survey Template
## Шаблон письма для сбора обратной связи от клиентов

### Body

Здравствуйте, $ClientName.

Благодарим за обращение в Службу поддержки по цифровой безопасности, организованную международной правозащитной организацией Access Now - https://accessnow.org.

Уведомляем, что Ваше обращение "{$Ticket-&gt;Subject}" решено.

Ваше мнение очень важно для нас. Если вы хотите поделиться своими пожеланиями по взаимодействию со Службой поддержки Access Now, пожалуйста, примите участие в опросе:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Номер Вашего обращения:  {$Ticket-&gt;id}

Если у Вас есть какие-либо вопросы или комментарии - сообщите нам, и мы постараемся Вам помочь.

Спасибо,

$IHName

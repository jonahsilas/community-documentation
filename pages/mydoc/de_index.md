---
title: "Dokumentation für die Gemeinschaft der Digital Security Helpline"
keywords: home
tags: [home]
layout: home
sidebar: mydoc_sidebar
permalink: de_index.html
ref: home
lang: de
---


Auf dieser Website finden Sie Anleitungen, die wir bei Access Now Digital Security zur Bearbeitung verschiedener Fälle während unseres 24/7-Betriebs verwenden.

Wenn Sie ein digitales Sicherheits-Helpdesk betreiben oder nur Ihren Freunden dabei helfen, ihre Online-Aktivitäten zu sichern oder auf digitale Sicherheitsvorfälle zu reagieren, finden Sie in diesen Anleitungen einige nützliche Tipps und Vorgehensweisen.

Wenn Sie eine Seite aktualisieren möchten, klicken Sie einfach auf "Edit Me" oder besuchen Sie dieses [Gitlab-Repository](https://gitlab.com/AccessNowHelpline/community-documentation).

{% include links.html %}



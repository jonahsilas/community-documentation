---
title: Handling of Fake Domains
keywords: fake domain, impersonation
last_updated: April 15, 2019
tags: [fake_domain, articles]
summary: "A fake domain has been established to impersonate a legitimate website of a client, and we need to report it."
sidebar: mydoc_sidebar
permalink: 342-Handling_Fake_Domains.html
folder: mydoc
conf: Public
lang: en
---


# Handling of Fake Domains
## What to do if a fake domain is reported by a client

### Problem

- A client's advocacy work is being used by an adversary to push their own agenda for their benefit/gain.

- A client's target audience is confused about which website contains the legitimate information.

- Some fake websites might be used for phishing or spreading malware.


* * *


### Solution

**Please note!** For security reasons, when escalating cases to the hosting or domain provider of the fake website, we should not include any information on the client and their organization.

#### Precautions

- Be careful with cloned/fake websites when analyzing and gathering evidence of impersonation, as they may contain malware. Do not visit the fake website, and use the online resources for URL scanning listed below to analyze it instead.

- Even if the client is trusted and vetted, we should evaluate carefully their reports of a cloned domain - in some cases their website might have been copied by former colleagues or other partners to spread its content or for other non-malicious reasons. We should therefore always check the `whois` of the website, as the owner might be a civil society member.
- These types of cases can involve legal procedures and court orders. Hosting and domain providers do not usually take down a website or remove content without receiving an order to comply with their policy and/or with the law. The legal notice which our client must submit to the hosting provider of the violating website to progress on the abuse report needs to be specific (i.e. contain information on the complainant or their attorney), and all required information needs to be in one notice, including the description of the violating content and links to the fake website. In some cases, our clients may not want to expose their organization due to security reason, so we should warn them in advance about this requirement.


#### Handling procedure

**Please remember not to visit the fake website with your normal browser and on your machine, as it might be infected - take precautions like using a virtual machine if you really need to visit the website. In any case, the following instructions should be sufficient for gathering the information you need.**

- Do an initial gathering of information without visiting the fake website:

    1. Run the `nslookup` command on the domain name or simply ping it.
    
    2. Run the `whois` command twice: with the IP address result in step 1 and with the domain name. The `whois` result for the IP address will include the hosting provider of the website, while the `whois` result for the domain name will include the domain provider.
    
    3. Take note of the following information: domain registrar, reseller, hosting provider, abuse email for domain and hosting provider.

- Conduct a manual test to check if the cloning is still ongoing by asking the client to upload new content and see if the same changes are applied to the fake website. **This is optional, and only needed if the fake website is an exact clone of the client's website**

- Do a search on MISP following the instructions in [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html) to check if any of the indicators in this attack have already been seen by the community.

- Use the following tools to figure out if the website is spreading malware or used for other malicious intents:
    - [circl.lu/urlabuse/](https://circl.lu/urlabuse/)
    - [www.virustotal.com](https://www.virustotal.com)
    - [sitecheck.sucuri.net/](https://sitecheck.sucuri.net/)
    - [urlscan.io/](https://urlscan.io/)
    - [www.threatcrowd.org](https://www.threatcrowd.org/)

- Additionally, check if [The Wayback Machine](https://archive.org/web/) has indexed the original and fake websites, as it might give proof of the cloning by showing the history of the original website.

- Before raising the report to the concerned authorities of the violating website, ask the client to sign off on the information that will be communicated to the hosting or domain provider. In some cases, the client might not want to expose their identity, so we should never provide their personal information to third parties without their consent.

- Proceed with the escalation report, you can find templates here:
    - [Report Impersonation/Cloning to Domain Provider](343-Report_Domain_Impersonation_Cloning.html)
    - If the website is used for spreading malware or for other attacks, you can use these templates:
        - [Article #259: Disable C&amp;C server - email to registrar of malicious domain](259-Disable_Malicious_Server_registrar.html)
        - [Article #260: Disable C&amp;C server - email to hosting provider](260-Disable_Malicious_Server_hosting_provider.html)
        - [Phishing Template](Report_Domain_Impersonation_Cloning_Phishing.html)

- Flag the cloned website to Google Safe Browsing, through these forms:
    - [General Report](https://safebrowsing.google.com/safebrowsing/report_general/)
    - [Report to be used if the fake website is spreading malware](https://safebrowsing.google.com/safebrowsing/report_badware/)
    - In the forms, provide additional information which can be useful to investigate, including results from online tools like urlscan.io. Also encourage the client to submit a report.

- If the reseller's information is included in the `whois` results for the fake website, escalate the issue to them too.

- An escalation to ICANN for complaints about the registrar can be done if the hosting and domain provider do not take action after our report. You can find a form for this kind of escalations [here](https://forms.icann.org/en/resources/compliance/complaints/registrars/standards-complaint-form).

- If there is any connection to the United States, you can use the Federal Trade Commission's fake website complaint [form](https://www.wikihow.com/Report-a-Fraud-Website#Reporting_the_Fraudulent_Website_sub).
- Finally, add an event to MISP following the instructions in [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html).


* * *


### Comments

- If the case is out of mandate, please see [Article #34: Handling of Fake Domains with Commercial Involvement](34-Handling_Fake_Domains_Commercial.html).

- You can also try reporting to the local CERT of the client's country or of the country where the website is registered.

    A list of CERTs per country can be found [here](http://first.org/members/map).
* * *


### Related Articles

- [Article #34: Handling of Fake Domains with Commercial Involvement](34-Handling_Fake_Domains_Commercial.html)
- [Article #259: Disable C&amp;C server - email to registrar of malicious domain](259-Disable_Malicious_Server_registrar.html)
- [Article #260: Disable C&amp;C server - email to hosting provider](260-Disable_Malicious_Server_hosting_provider.html)
- [Article #343: Fake Domain - Report Impersonation/Cloning to Domain Provider](343-Report_Domain_Impersonation_Cloning.html)
- [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html)
- [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html)

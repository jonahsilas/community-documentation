---
title: Initial Reply - For Italian Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Italian speakers"
sidebar: mydoc_sidebar
permalink: 264-Initial_Reply_For_Italian_Speakers.html
folder: mydoc
conf: Public
ref: Initial_reply
lang: it
---


# Initial Reply
## First response, Email to Client for Italian speakers
### Body

Gentile $ClientName,

grazie per aver contattato la Digital Security Helpline di Access Now (https://www.accessnow.org/help). Mi chiamo $IHNAME e sono a disposizione per assisterti.

Abbiamo ricevuto la tua richiesta e il nostro gruppo la sta analizzando in dettaglio.

Ti informiamo che la Helpline opera con un gruppo di professionisti di sicurezza che lavorano in diverse fasce orarie, Per questo motivo, diverse persone potrebbero risponderti a seconda dell'ora e del giorno in cui la richiesta è stata ricevuta. Io o uno dei miei colleghi e colleghe ti contatteremo a breve per richiedere maggiori dettagli o più informazioni per comprendere meglio la situazione.

Per favore continua a includere "[accessnow #ID]" nell'oggetto della tua email per tutte le future comunicazioni relative a questa richiesta. Questo numero è l'identificativo che il nostro sistema di ticketing assegna automaticamente per gestire e coordinare le richieste.

Saluti

$IHNAME


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)

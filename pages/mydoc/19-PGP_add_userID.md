---
title: PGP - Add UserID
keywords: email, PGP, user ID, key management, Enigmail, Thunderbird, email security
last_updated: November 5, 2018
tags: [secure_communications, articles]
summary: "A user needs to add a new UserID to their current PGP Key, because they have a new email address or have moved to a different job."
sidebar: mydoc_sidebar
permalink: 19-PGP_add_userID.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Add UserID
## How to add a User ID to a PGP Key

### Problem

A client would like to add a new user ID to their current PGP Key and doesn't know how to do it.

The client might be unaware of the risks implied by connecting more than one identity to a PGP key published in the key servers.


* * *


### Solution

Before we proceed to explain how to add a new user ID to a PGP key, we should make sure that the client understands that by doing this they will connect their identities to a PGP key that is publicly accessible in the key servers. If they would like to keep the two identities separated, we should recommend they create a new PGP key for the new user ID.

If the client finds it acceptable to connect the new identity to the old one, then we can proceed with the following steps:

1. Send the client an email with instructions based on [Article #9: PGP - add User ID - email template](9-PGP_add_userid_email.html).

2. Check if the updated key has been published in the keyservers, for example [here](http://zimmermann.mayfirst.org/).

3. Close the case by sending an email based on the template in [Article #20: PGP - User ID added](20-PGP-UserID_added.html). 

* * *


### Comments



* * *


### Related Articles

- [Article #9: PGP - add User ID - email template](9-PGP_add_userid_email.html)
- [Article #20: PGP - User ID added](20-PGP-UserID_added.html)

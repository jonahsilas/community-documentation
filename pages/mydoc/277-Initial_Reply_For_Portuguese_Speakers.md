---
title: Initial Reply - For Portuguese Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Portuguese speakers"
sidebar: mydoc_sidebar
permalink: 277-Initial_Reply_For_Portuguese_Speakers.html
folder: mydoc
conf: Public
ref: Initial_reply
lang: pt
---


# Initial Reply in Portuguese
## First response, Email to Client that speaks Portuguese

### Body

Olá $ClientName,

Obrigado por entrar em contato com a Linha de Ajuda de Segurança Digital do Access Now (https://www.accessnow.org/linha-de-ajuda-em-seguranca-digital/), meu nome é $IHName e estou aqui para ajudar.

Recebemos sua solicitação inicial e minha equipe está trabalhando nisso. 

Por favor, saiba que a Linha de Ajuda é operada por um grupo de profissionais em segurança digital que trabalhan em diferentes fusos horários. Portanto, diferentes membros da equipe podem responder às suas mensagens, dependendo da hora e do dia em que foram recebidas. Enviaremos um email em breve com detalhes adicionais ou perguntas que possamos precisar.

Por favor, continue a incluir “[accessnow #ID]” na linha de assunto para todas as futuras correspondências sobre este assunto. Este é um ID de referência gerado pelo nosso sistema para gerenciar e coordenar tarefas para sua solicitação.

Obrigado,

$IHName


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)

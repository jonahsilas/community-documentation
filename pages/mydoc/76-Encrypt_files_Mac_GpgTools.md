---
title: Encrypt Files on a Mac with GpgTools
keywords: file encryption, Mac, GpgTools, backup
last_updated: August 27, 2019
tags: [devices_data_security, articles]
summary: "A client using macOS is uploading sensitive data on an online file hosting service (DropBox-like) without protecting it through encryption."
sidebar: mydoc_sidebar
permalink: 76-Encrypt_files_Mac_GpgTools.html
folder: mydoc
conf: Public
ref: Encrypt_files_Mac_GpgTools
lang: en
---


# Encrypt Files on a Mac with GpgTools
## Use GpgTools to protect data 

### Problem

- Sensitive information is being sent or stored online with no protection or encryption.
- Sensitive files need to be protected in case a computer or storage device is stolen or lost or there is a break-in in the client's office.
- Backups are currently stored with no protection or encryption.
- Need to send sensitive data to a specific person encrypting the data with their public key and uploading it to a hosting service.


* * *


### Solution

This article is dedicated to specific cases where GPG is to be used for encrypting files. For other options, please see [Article #210: File Encryption with Veracrypt or GPG](210-File_Encryption_VeraCrypt_GPG.html).

Download and install GpgTools.

- [Download link](https://releases.gpgtools.org/GPG%20Suite%20-%202014.11.Yosemite-b3.dmg)

Use the contextual menu (that appears by right-clicking on files you want to
encrypt) as shown in this [tutorial](http://support.gpgtools.org/kb/gpgservices-faq/how-to-encrypt-and-sign-text-or-files-with-gpgservices)

Help the client practice encrypting and decrypting files. 

Follow up with an email in case further details need to be clarified.


* * *


### Comments



* * *

### Related Articles

- [Article #182: FAQ - Secure Backup](182-Secure_Backup.html)
- [Article #210: File Encryption with GPG or VeraCrypt](210-File_Encryption_GPG_VeraCrypt.html)

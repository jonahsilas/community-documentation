---
title: FAQ - Data Leak
keywords: leak, data leak
last_updated: November 7, 2018
tags: [data_leaks, articles, faq]
summary: "Sensitive information that was stored in a client's computer or storage has been published on a public website without the client's consent and is being used against the client or their organization."
sidebar: mydoc_sidebar
permalink: 22-FAQ-Data_Leak.html
folder: mydoc
conf: Public
lang: en
---


# FAQ - Data Leak
## Mitigation steps after private documents have been released to the public

### Problem

A confidential document that was restricted to certain individuals and was stored in personal computers or in a shared online group has been leaked to the public and is being used or can be used to harm the client or their organization.


* * *


### Solution

The first question we should ask is:

Who can be affected by the leak? Is there someone at risk that needs to be protected?

Once you have made sure that the individuals who might be harmed by the leak are protected, you can proceed with the investigation, starting from the following questions:

1. Perform a risk assessment. Who are the possible adversaries?
2. Investigate possible weak points on the information storage or transfer system.
3. Are there any symptoms of a recent account hacking on a member of the group? 
4. Has any device belonging to a member of the group been lost or stolen?
5. Investigate how the information was transferred/shared.
6. Was it published on Google drive, Dropbox, Google groups, or similar? Had this group set up the permissions appropriately? 
7. Are there any logs that might help in this process?


* * *


### Comments

- Obtaining information from web services such as Google, Dropbox or similar is difficult without a legal process in place.

- In addition to tracking the leak, look for opportunities to improve the organization's digital security practices.

- If the published information was available online, this might be a case of doxing. See [Article #298: Doxing and Non-Consensual Publication of Pictures and Media](298-Doxing_Non-Consensual_Publication.html).


* * *


### Related Articles

- [Article #298: Doxing and Non-Consensual Publication of Pictures and Media](298-Doxing_Non-Consensual_Publication.html)

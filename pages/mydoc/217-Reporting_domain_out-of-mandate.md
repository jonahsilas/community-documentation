---
title: Reporting a Domain Name in Out-of-Mandate Cases
keywords: fake domain, impersonation, cloning, escalations, domain provider, take-down request, out-of-mandate
last_updated: March 4, 2019
tags: [fake_domain_templates, templates] 
summary: "This template is sent as guidance to clients who are reporting a domain name but are out of mandate"
sidebar: mydoc_sidebar
permalink: 217-Reporting_domain_out-of-mandate.html
folder: mydoc
conf: Public
lang: en
---


# Reporting a Domain Name in Out-of-Mandate Cases
## This template is sent as guidance to clients who are reporting a domain name but are out of mandate

### Body

Hi XXXXX,

I hope this email finds you well.

I'm very sorry to hear about your incident. Access Now Helpline team helps civil society members and non-profit organizations in digital security concerns and incidents and unfortunately we cannot assist requests that are not coming from the civil society, so we can only offer instructions for you to take action to deactivate the fake domain.

The management of a domain is handled by the relevant registrar. You can directly report your claim to the registrar and they could investigate the incident further and take appropriate action.

You can get the registrar information of any website by doing a search on this website: https://who.is/

This site will look up all the relevant information for a domain name, including the contacts to reach out to in this situation.

In this case, the following information is provided for the domain "$Domain name":

- Registrar: XXXXXXXX
- Registrant Contact Information: Name: XXXXXXXX
- Email: XXXXXXXX

I would suggest that you report this incident through the registrar by sending an email to: $abuse-contact@registrar-domain

Depending on what country you are based in, there might be Computer
Emergency Response Team (CERT) able to assist. Please see below a
list of national CERTs and their contact details:

https://www.cert.org/incident-management/national-csirts/national-csirts.cfm

You can also try and report the abusing domain on Google Safe Browsing, through this form: https://safebrowsing.google.com/safebrowsing/report_general/

I hope you find this information useful. In case you have any
question please let us know.

Kind Regards,

[IH's Name]


* * *


### Related Articles

[Article #34: Handling Fake Domains in Out-of-Mandate Cases](34-Handling_Fake_Domains_Out-of-Mandate.html)

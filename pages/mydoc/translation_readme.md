---
title: How to Translate This Website
summary: "Guide for the localization of this website"
sidebar: mydoc_sidebar
permalink: translation_readme.html
folder: mydoc
conf: Public
ref: translation_readme
lang: en
---


# How to Translate This Website

## Adding a New Language to the Website

At the moment, this website is localized for 9 languages. If you want to add a new language, you need to edit the file:

	community-documentation/_layouts/default.html	

and add the new language to this list:

    var languages = [ar, de, en, es, fl, fr, it, pt, ru];

You will also need to add to this stylesheet:

	community-documentation/css/customstyles.css

adding the new language to this line:

```
.ar:lang(ar), .de:lang(de), .en:lang(en), .es:lang(es), .fl:lang(fl), .fr:lang(fr), .it:lang(it), .pt:lang(pt), .ru:lang(ru)
```

## Adding a Localized Home Page

To have the home page localized in a new language, you need to copy the file `index.html` (in the root directory) to a new file in the `pages/mydoc` folder. The new file will be named with a 2-letter language code preceding the file name, for example, for Italian: `it_index.md`.

Apart from translating the body of the page, you will also need to modify the following fields:

- `title:` - the localized title of the page
- `permalink:` - this will need to be identical to the file name, for example, if the file name is `it_index.md`, the permalink will need to be: `it_index.html`
- `lang:` - the value needs to be the 2-letter code for the language of the page.

You will also need to add the localized title of the website in the `_config.yml` file in the root directory, adding it to this menu:

```
topnav_title: multilanguage
m_topnav_title:
  en: Access Now Digital Security Helpline Public Documentation
  it: Documentazione pubblica della Helpline di sicurezza informatica di Access Now
  ru: Документация сообщества Службы поддержки по цифровой безопасности Access Now
  es: Documentación para la comunidad de la Línea de ayuda en seguridad digital de Access Now
  fr: Documentation de communauté de la plateforme d’assistance numérique d'Access Now
```

## Creating and Editing a Page

If you are adding a localized page, create the file by copying it from the relevante page in another language or from one of the relevant templates (for [articles](https://gitlab.com/AccessNowHelpline/community-documentation/blob/master/pages/mydoc/article_template.md) and [email templates](https://gitlab.com/AccessNowHelpline/community-documentation/blob/master/pages/mydoc/email_template.md)), and modify the following tags in the YAML front matter at the beginning of the document:

* `ref:` - This should be a common one-word unique identifier that is repeated in all localized versions of the same page. In order for one page to appear as being the translation of another page, both pages need to have the same identifier in this field. So if you copied the new file from the page you are translating, there will be no need to modify this value.
* `lang:` - This should be a 2-letter code corresponding to the language of the document.


## Adding a Tag Page

If you want a tag page to be localized, you need to create a new page in the `pages/tags` folder.

You can start from the English template, for example:

	tag_browsing_security.md

Copy the English template to a new file, and rename it by adding the relevant 2-letter language code, for example you will add "it_" for Italian:

	it_tag_browsing_security.md

The YAML front matter of the file also needs to be modified with the relevante language code, for example, for Italian:

* lang: it
* langNow: it

Check that the permalink matches the title (if the file is named `it_tag_browsing_security.md`, the permalink will be `it_tag_helpline_procedures_templates.html` and translate the title value in the YAML front matter, and you're set.


## Adding an Item to the Side Panel

### Adding a Language to an Item

If you want an item to be localized, you need to modify the side panel configuration file:

	community-documentation/_data/sidebars/mydoc_sidebar.yml

In the file, identify the item you want to add a language to and add the 2-letter language code in the list of languages for that item.


### Localizing the Title of an Item

To localize the title of an item, you will need to modify the side panel configuration file:

	community-documentation/_data/sidebars/mydoc_sidebar.yml

You will need to:

- Change the value for `title:` to "multilanguage" (i.e.: `title: multilanguage`)
- Add a localized title for each language, as in the following example:

```
- title: multilanguage
    en: Articles
    it: Articoli
    es: Artículos
    ru: Статьи
    fr: Articles
```

## Adding a Localized Link to the Top Navigation Menu

To add a localized link to the top navigation menu, you need to edit the menu configuration file:

	community-documentation/_data/topnav.yml

You will need to add:

- Change the value for `title:` to "multilanguage" (i.e.: `title: multilanguage`)
- Add a localized Title for each language, as in the following example:

```
- title: multilanguage
     en: About
     it: Chi siamo
     ru: О нас
     fr: À propos
```

Links can also be localized, following these instructions:

- Change the value for `external_url:` to "multilanguage" (i.e.: `external_url: multilanguage`)
- Add a key `m_external_url:` followed by a list of localized strings for each language, as in the example below:

<pre>
        - title: multilanguage 
          en: Digital Security Helpline
          it: Helpline di sicurezza digitale
          external_url: multilanguage
          m_external_url: 
            en: https://accessnow.org/help
            it: https://accessnow.org/help
            de: https://www.accessnow.org/help-deutsch/?ignorelocale
                         
</pre>


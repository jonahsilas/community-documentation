---
title: How to set up Deflect DDoSP for a client
keywords: DoS, DDoS, DDoSP, DDoS protection, Deflect
last_updated: Aug 02, 2018
tags: [ddos_attack, articles]
summary: "The client has suffered or is currently suffering a denial-of-service attack. The client suspects their website might be targeted by DDoS attacks in the future and would like to set up preventative protection."
sidebar: mydoc_sidebar
permalink: 137-Set_Up_Deflect.html
folder: mydoc
conf: Public
lang: en
---


# How to set up Deflect DDoSP for a client
## Steps for setting up Deflect DDoS protection

### Problem

The client manages a website that is under DDoS attack or could be targeted by DDoS attacks.


* * *

### Solution

If, after we have presented them the different options for protection against denial-of-service attacks, the client decides to use Deflect, the steps presented below should be followed:

1. Gather information about the goal and focus of the website. Do these fit Deflect's requirements and eligibility criteria? Check [this page](https://docs.deflect.ca/en/latest/eligibility.html).

2. If the client is eligible according to Deflect's criteria, send an email to the client pointing them to [Deflect's sign-up page](https://dashboard.deflect.ca/signup) and tell them to follow the set up instructions on Deflect's dashboard.

    A step-by-step guide on how to set up Deflect can be found [here](https://docs.deflect.ca/en/latest/dashboard_walkthrough/walkthrough.html)

3. When the initial configuration is done, the client will be asked to update the DNS entries for their domain. If the client needs assistance with this, we can refer them to [this page](http://wiki.gandi.net/en/dns/change).

4. Verify the client has changed the DNS settings. You can use nslookup and whois for this task.

5. Confirm with the client that the DDoS protection is active and answer any questions.

* * *


### Comments

First check Deflect's [eligibility criteria](https://docs.deflect.ca/en/latest/eligibility.html). We are vettors, and need to make sure we support the correct participants on Deflect's project.

At the time of writing this article there were some bugs on Deflect's dashboard when uploading the SSL cert files. If the clients encounter this type of issue we may need to assist them to create a ticket either from the dashboard or by writing to sysops@equalit.ie
